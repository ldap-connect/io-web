import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServerService } from '../server.service'

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  userForm: FormGroup;
  json :any;
  objectClasses = ["top","person","inetOrgPerson"];
  showMsg: boolean=false;

  constructor(private fb: FormBuilder,private _service: ServerService) { 
    this._service.afterEvent()
    .subscribe(data => {
      this.json=data;
      console.log(this.json);
      if(this.json.error){
        this.showMsg=true;
      }
    });
  }

  ngOnInit() {

    this.userForm = this.fb.group({
      commonName : [''],
      surName : [''],
      employeeNumber :[''],
      email:['']
    });
  }

  OnSubmit()
  {
    console.log( this.userForm.value);
    this._service.onEvent( {method:"createUser" , arg:this.userForm.value});
  }
}
