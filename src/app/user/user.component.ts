import { Component, OnInit } from '@angular/core';
import { ServerService } from '../server.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers:[ServerService]
})
export class UserComponent implements OnInit {

  eventValue :String;
  json :any;

  constructor(private _service :ServerService){
    
    this._service.afterEvent()
    .subscribe(data => this.json=data);
    
  }

  ngOnInit() {
    this._service.onEvent( {method : "getUsers", arg:""} );

  }
  
}
