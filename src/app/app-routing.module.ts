import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { CreateUserComponent } from './user/create-user.component';

const routes: Routes = [

  {path :'getUsers' ,component : UserComponent},
  {path :'createUser' ,component : CreateUserComponent},
  {path : '' ,redirectTo:'getUsers' , pathMatch:'full' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
