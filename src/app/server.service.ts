import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor() { }

  private socket = io("http://localhost:3000");

  onEvent(data)
  {
    console.log("method:"+data.method);
    this.socket.emit('callMethod',data);
  }

  afterEvent()
  {
    let observable = new Observable<{}>(observer=>{
      this.socket.on('emitEvent', (data)=>{
          observer.next(data);
      });
      return () => {this.socket.disconnect();}
  });
  return observable;
  }

  checkStatus(data)
  {
    console.log(" ckeckstatus() ::1 ::");
    this.socket.emit('status',data);
  }

  getStatus()
  {
    console.log("getstatus :: 1")
    let observable = new Observable<{}>(observer=>{
      this.socket.on('checkStatus', (data)=>{
        observer.next(data);
         
      });
      return () => {this.socket.disconnect();}
  });
  return observable;

}
  
}
